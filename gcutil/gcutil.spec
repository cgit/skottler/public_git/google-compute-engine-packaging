Name:           gcutil
Version:        1.11.0
Release:        1
License:        Apache 2
Summary:        Google Compute Engine utilities
URL:            https://code.google.com/p/google-compute-engine-tools/
Group:          System/Management
Source0:        https://google-compute-engine-tools.googlecode.com/files/%{name}-%{version}.tar.gz
Requires:       python
Requires:       python-ipaddr
Requires:       python-httplib2
Requires:       python-iso8601
Requires:       python-gflags
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root
BuildArch:      noarch

%description
gcutil is a tool that allows you to interact with your Google Compute Engine
resources through the command-line.

%prep
%setup -q -n %{name}-%{version}

%build

%install
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/gcutil
chmod +x  gcutil
rm -rf lib/{python_gflags,ipaddr,httplib2,iso8601}
cp -r gcutil lib $RPM_BUILD_ROOT/%{_datadir}/gcutil
mkdir -p $RPM_BUILD_ROOT/%{_bindir}
ln -s %{_datadir}/gcutil/gcutil $RPM_BUILD_ROOT/%{_bindir}/gcutil

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc CHANGELOG LICENSE VERSION
%dir %{_datadir}/gcutil
%{_datadir}/gcutil/*
%{_bindir}/gcutil

%changelog
